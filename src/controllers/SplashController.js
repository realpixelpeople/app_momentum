import {MomentumAPI} from '../infra/external'
import {MomentumDAO} from '../infra/internal'

export default class SplashController {

    constructor(onLoadCards, onError, onProgress) {
        this.onLoad = onLoadCards
        this.onError = onError
        this.onProgress = onProgress
        this.momentumAPI = new MomentumAPI()
        this.momentumDAO = new MomentumDAO()
        that = this
    }

    callApi() {
        this.momentumAPI.requestEnterprise(this.onLoadRequest, this.onError, this.onProgress)
    }

    onLoadRequest(value) {
        const items = []
       for (let index = 0; index < value.empreendimentos.length; index++) {
           const element = value.empreendimentos[index];
           const img = element.logo && element.logo ? element.logo['1125x2436'] : ""

           const cardItem = {
               id : element.id,
               nome : element.nome,
               logo : img
           }
           items.push(cardItem)
       }
       that.onLoad(items)
    }

    onError(error) {
       // Call Db 
    }

   onProgress(percentComplete) {
       console.log("percent", percentComplete)
   }
}