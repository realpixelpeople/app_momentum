import {MomentumAPI} from '../infra/external'
import {MomentumDAO} from '../infra/internal'

export default class HomeController {

    constructor(onLoadCards, onError) {
        this.onLoad = onLoadCards
        this.onError = onError
        this.momentumAPI = new MomentumAPI()
        this.momentumDAO = new MomentumDAO()
        that = this
    }

    callApi() {
        this.momentumDAO.getItems(this.onResolve.bind(this))
       
    }

    onResolve(items) {
        if(items != null) {
            this.onLoadRequest(items)
            
        } else {
            this.momentumAPI.requestEnterprise(this.onLoadRequest, this.onError)
        }
    }

    onLoadRequest(value) {
        
       const items = []
       for (let index = 0; index < value.empreendimentos.length; index++) {
           const element = value.empreendimentos[index];
           const img = element.logo && element.logo ? element.logo[Object.keys(element.logo)[0]] : ""

           const cardItem = {
               id : element.id,
               nome : element.nome,
               logo : img,
               bg: element.bg[Object.keys(element.bg)[0]]
           }
           items.push(cardItem)
       }
       that.onLoad(items)
    }

    onError(error) {
       // Call Db 
    }

    pushItems(value) {
        
    }
}