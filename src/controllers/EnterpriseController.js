import {MomentumAPI} from '../infra/external'
import {MomentumDAO} from '../infra/internal'

export default class EnterpriseController {

    constructor(enterprise, onLoadItems) {
        this.onLoadItems = onLoadItems.bind(this)
        this._DAO = new MomentumDAO()
        this.callback = this.onReceiveValues.bind(this)
        this._DAO.restoreItemById(enterprise.id, this.callback)
    }    

    onReceiveValues(values) {
        this.onLoadItems(values)
    }
}
