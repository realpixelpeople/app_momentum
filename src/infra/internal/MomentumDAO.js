import {AsyncStorage, Platform} from 'react-native'
import {DATABASE_NAME, DATABASE_CORRETOR_NAME, DATABASE_IMAGE_NAME} from './DAOConstants'
var RNFS = require('react-native-fs');
import RNFetchBlob from 'rn-fetch-blob'

const DIRECTORY_PATH_LOCAL = Platform.select({
    ios: () => `${RNFS.DocumentDirectoryPath}`,
    android: () => `${RNFS.DocumentDirectoryPath}`
})();

const DIRECTORY_PATH_EXTERNAL = Platform.select({
    ios: () => `${RNFS.DocumentDirectoryPath}`,
    android: () => `${RNFS.DocumentDirectoryPath}`
})();

export default class MomentumDAO {

    constructor() {
        this._prepareDirExternal()
        this._prepareDirLocal()
    }

    storeEnterprise(enterprise) {
        try {
            const values = JSON.stringify(enterprise)
            AsyncStorage.setItem(DATABASE_NAME, values, (error) => {
            })
        } catch(e) {
            // Not saved    
        }
    }

    getItems(onResolve) {
        try {
            AsyncStorage.getItem(DATABASE_NAME, (error, results) => {
                if(results != null) {
                    const values = JSON.parse(results)
                    onResolve(values)
                } else {
                    onResolve(null)
                }
            })
        } catch(e) {
            onResolve(null)
        }
    }

    getTopics(onResolve, id, topicId, type) {
        try {
            restoreItemById(id, (items) => {
                const topics = items.filter((v) => {
                    return v.id == topicId
                })

                const results = []
                if(type == "fotos") {
                    topics["fotos"][Object.keys(topics["fotos"])[0]].forEach(element => {
                        element.type = "fotos"
                        results.push(element)
                    });
                }

                if(type == "videos") {
                    topics["videos"][Object.keys(topics["videos"])[0]].forEach(element => {
                        element.type = "videos"
                        results.push(element)
                    });
                }

                if(type == "tour") {
                    topics["tour360"][Object.keys(topics["tour360"])[0]].forEach(element => {
                        element.type = "tour"
                        results.push(element)
                    });
                }

                if(type == "all") {
                    topics["tour360"][Object.keys(topics["tour360"])[0]].forEach(element => {
                        element.type = "tour"
                        results.push(element)
                    });

                    topics["videos"][Object.keys(topics["videos"])[0]].forEach(element => {
                        element.type = "videos"
                        results.push(element)
                    });

                    topics["fotos"][Object.keys(topics["fotos"])[0]].forEach(element => {
                        element.type = "fotos"
                        results.push(element)
                    });
                }

                onResolve(results)
            })
        } catch(e) {
            onResolve(null)
        }
    }

    restoreItemById(id, cb) {
        try {
            AsyncStorage.getItem(DATABASE_NAME, (error, results) => {
                if(results != null) {
                    const values = JSON.parse(results)
                    const items = values.empreendimentos.filter((value) => {
                        return value.id === id
                    })
                    cb(items)
                }
            })
        } catch(e) {
            cb(null)
        }
    }


    getCorretorGallery(cb) {
        try {
            AsyncStorage.getItem(DATABASE_CORRETOR_NAME, (error, results) => {
                if(results != null) {
                    const values = JSON.parse(results)
                    const filtered = []
                    values.forEach(item => {
                        item.isSelected = false
                        filtered.push(item)
                    })
                    cb(filtered)
                } else {
                    cb([])
                }
            })
        } catch(e) {
            cb([])
        }
    }

    storeImages(url, type, cb) {
        let fileName = ""
        url.split("/").forEach((element, index) => {
            if(index != 0) fileName = `${fileName}${element.replace(".", "")}` 
        })

        const p = type == "internal" ? `${DIRECTORY_PATH_LOCAL}/${fileName}.jpg` : `${DIRECTORY_PATH_EXTERNAL}/${fileName}.jpg`


        RNFetchBlob.fs.exists(p)
        .then((exist) => {
            if(exist) {
                cb(p)
            } else {
                RNFetchBlob
                .config({
                    path: p,
                    fileCache : true
                }).fetch("GET", url, {}).then(res => {
                    cb(res.path())
                }).catch(err => {})
            }
        })
        .catch(() => { 
            RNFetchBlob
            .config({
                path: p,
                fileCache : true
            }).fetch("GET", url, {}).then(res => {
                cb(res.path())
            }).catch(err => {})
        })

       
    }

    storeVideo(url, type, cb) {
        let fileName = ""
        url.split("/").forEach((element, index) => {
            if(index != 0) fileName = `${fileName}${element.replace(".", "")}` 
        })
        
        const p = type == "internal" ? `${DIRECTORY_PATH_LOCAL}/${fileName}.mp4` : `${DIRECTORY_PATH_EXTERNAL}/${fileName}.mp4`
       
        RNFetchBlob.fs.exists(p)
        .then((exist) => {
            if(exist) {
                cb(p)
            } else {
                RNFetchBlob
                .config({
                    path: p,
                    fileCache : true
                }).fetch("GET", url, {}).then(res => {
                    cb(res.path())
                }).catch(err => {})
            }
        })
        .catch(() => { 
            RNFetchBlob
            .config({
                path: p,
                fileCache : true
            }).fetch("GET", url, {}).then(res => {
                cb(res.path())
            }).catch(err => {})
        })

       
    }

    storeLocal(uri, option, type, cb) {
        let fileName = ""
        uri.split("/").forEach((element, index) => {
            if(index != 0) fileName = `${fileName}${element.replace(".", "")}` 
        })
        const extension = option == "fotos" ? ".jpg" : ".mp4"
        const p = type == "internal" ? `${DIRECTORY_PATH_LOCAL}/${fileName}${extension}` : `${DIRECTORY_PATH_EXTERNAL}/${fileName}${extension}`


        RNFetchBlob.fs.exists(p)
        .then((exist) => {
            if(exist) {
                cb(p)
            } else {
                RNFetchBlob.fs.cp(uri, p).then(copy => {
                    cb(p)
                }).catch(err => {})
            }
        })
        .catch(() => { 
            RNFetchBlob.fs.cp(uri, p).then(copy => {
                cb(p)
            }).catch(err => {})
        })
    }

    storeLocalImage(uri, type, cb) {
        let fileName = ""
        uri.split("/").forEach((element, index) => {
            if(index != 0) fileName = `${fileName}${element.replace(".", "")}` 
        })
        const p = type == "internal" ? `${DIRECTORY_PATH_LOCAL}/${fileName}.jpg` : `${DIRECTORY_PATH_EXTERNAL}/${fileName}.jpg`


        RNFetchBlob.fs.exists(p)
        .then((exist) => {
            if(exist) {
                cb(p)
            } else {
                RNFetchBlob.fs.cp(uri, p).then(copy => {
                    cb(p)
                }).catch(err => {})
            }
        })
        .catch(() => { 
            RNFetchBlob.fs.cp(uri, p).then(copy => {
                cb(p)
            }).catch(err => {})
        })
    }

    storeLocalFiles(files, cb) {
        try {
            const values = JSON.stringify(files)
            AsyncStorage.setItem(DATABASE_CORRETOR_NAME, values, (error) => {
            })
        } catch(e) {
            // Not saved    
        }
    }

    deleteLocalValues(values) {
        try {
            if(values != null && values.length > 0) {
                values.forEach(v => {
                    const path = v.uri 
                    RNFetchBlob.fs.exists(path).then(exist => {
                        if(exist) {
                            RNFetchBlob.fs.unlink(path).then(s => {

                            }).catch(err => {
                                
                            })
                        }
                    })
                })
            }
        } catch(e) {

        }
    }

    getStoredLocalFiltes(cb) {
        try {
            AsyncStorage.getItem(DATABASE_CORRETOR_NAME, (error, results) => {
                if(results != null) {
                    const values = JSON.parse(results)
                    cb(values)
                } else {
                    cb([])
                }
            })
        } catch(e) {
            cb([])
        }
    }

    storeLocalVideo(uri, type, cb) {
        let fileName = ""
        uri.split("/").forEach((element, index) => {
            if(index != 0) fileName = `${fileName}${element.replace(".", "")}` 
        })

        const p = type == "internal" ? `${DIRECTORY_PATH_LOCAL}/${fileName}.mp4` : `${DIRECTORY_PATH_EXTERNAL}/${fileName}.mp4`
         RNFetchBlob.fs.exists(p)
        .then((exist) => {
            if(exist) {
                cb(p)
            } else {
                RNFetchBlob.fs.cp(uri, p).then(copy => {
                    cb(p)
                }).catch(err => {})
            }
        })
        .catch(() => { 
            RNFetchBlob.fs.cp(uri, p).then(copy => {
                cb(p)
            }).catch(err => {})
        })
    }

    _prepareDirLocal() {
        try {
            RNFetchBlob.fs.mkdir(DIRECTORY_PATH_LOCAL)
                .then(() => {  })
                .catch((err) => { })
        } catch(e) {

        }
    }
    
    _prepareDirExternal() {
        try {
            RNFetchBlob.fs.mkdir(DIRECTORY_PATH_EXTERNAL)
                .then(() => {  })
                .catch((err) => { })
        } catch(e) {

        }
    }
}