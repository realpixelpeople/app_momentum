import axios from 'axios'
import { NETWORK_PROPERTIES, NETWORK_HEADERS } from './NetworkConstants' 

export default class Network {
    
    provideAxios() {
        const instance = axios.create({
            baseURL: NETWORK_PROPERTIES.BASE_URL,
            timeout: NETWORK_PROPERTIES.TIMEOUT,
            headers: NETWORK_HEADERS
        })
        return instance
    }
}