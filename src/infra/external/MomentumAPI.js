import Network from './Network'
import { NETWORK_PROPERTIES } from './NetworkConstants'
import { MomentumDAO } from '../internal'

export default class MomentumAPI {

    constructor() {
        this.API = new Network().provideAxios()
        this.DAO = new MomentumDAO()
    }

    // https://appmomentum.tpix.com.br/api/empreendimento
    requestEnterprise(onSuccess, onError, onProgress) {
        let config = {
            onUploadProgress: progressEvent => {
                let percentCompleted = Math.floor((progressEvent.loaded * 100) / progressEvent.total);
                let total = progressEvent.total;
                onProgress(percentCompleted)
            }
        }

        this.API
            .get(NETWORK_PROPERTIES.ENTERPRISE, config)
            .then(response => {
                const { data } = response
                console.log("response", data)
                if (response.status == 200) {
                    onSuccess(data)
                    this.DAO.storeEnterprise(data)
                } else {
                    onError()
                }
            }).catch(error => {
                onError(error)
            })
    }
}