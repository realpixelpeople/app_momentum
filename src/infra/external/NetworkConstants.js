export const NETWORK_PROPERTIES = {
    API_HEADER: 'X-API-KEY',
    API_KEY: '703c972efbba0db30d1af5efe8b80d800083aea3',
    BASE_URL: 'https://appmomentum.tpix.com.br/api/',
    TIMEOUT: 250000, // 25 seconds,
    ENTERPRISE: 'empreendimento'
}

export const NETWORK_HEADERS = { [NETWORK_PROPERTIES.API_HEADER] : NETWORK_PROPERTIES.API_KEY }
