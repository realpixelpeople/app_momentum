const FONT_STYLE = {
    NORMAL: 'OpenSans-Regular',
    THIN: 'OpenSans-Light',
    BOLD: 'OpenSans-Bold'
}

export default FONT_STYLE