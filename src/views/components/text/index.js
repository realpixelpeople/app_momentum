import AppTextBold from './AppTextBold'
import AppText from './AppText'
import AppTextThin from './AppTextThin'

export {
    AppTextThin,
    AppText,
    AppTextBold
}