import React, {Component} from 'react';
import { Text } from 'react-native';
import FontFamily from './AppFontFamily'

export default class AppTextBold extends Component {

    render() {
        const size = this.props.fontSize != null ? this.props.fontSize : 16
        const fontColor = this.props.fontColor != null ? this.props.fontColor : 'black'
        
        return(
            <Text style={[{fontFamily: FontFamily.BOLD, fontSize: size, color: fontColor }, this.props.style]}>
                {this.props.children}
            </Text>
        )
    }
}