
import {StyleSheet, Dimensions} from 'react-native'
import Colors from '../../../utils/AppColors'
const width = Dimensions.get('window').width ; 
const height = Dimensions.get('window').height ; 

const styles = StyleSheet.create({
    containerPagination: {
        backgroundColor: 'rgba(255, 255, 255, 0)'
    },
    dot: {
        width: 10,
        height: 10,
        borderRadius: 5,
        marginHorizontal: 8,
        backgroundColor: 'rgba(255, 255, 255, 0.92)'
    },
    inactiveDot: {
        width: 10,
        height: 10,
        borderRadius: 5,
        marginHorizontal: 8,
        borderColor: Colors.WHITE,
        borderWidth: 1,
        backgroundColor:  'rgba(0, 0, 0, 0)'
    },
    carousel: {
        height: height / 3,
        width
    },
    itemContainer: {
        height: height / 6,
        margin: 10, 
        backgroundColor: 'transparent', 
        flexGrow: 0 
    },
    cardContainer: {
        height: (height / 6) - 10,
        padding: 10
    },
    image: {
        height: (height / 6) - 20,
        width: null,
        resizeMode: 'contain'
    }
})

export default styles