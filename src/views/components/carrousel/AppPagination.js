import { Pagination } from 'react-native-snap-carousel';
import React, {Component} from 'react';
import {Platform,  TouchableOpacity , Text, View} from 'react-native';
import Styles from './Styles'

export default class AppPagination extends Component {

    constructor(props) {
        super(props)
        this.state = {
            length: props.length ? props.length : 0,
            activeSlide: 0
        }
    }

    updateActiveSlide(index) {
        this.setState({ activeSlide: index })
    }

    updateLength(size) {
        this.setState({ length: size })
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.length != null && nextProps.length != this.state.length) {
            this.setState({ length: nextProps.length })
        }
    }

    render() {
        return (
            <Pagination
                dotsLength={this.state.length}
                activeDotIndex={this.state.activeSlide}
                containerStyle={Styles.containerPagination}
                dotStyle={Styles.dot}
                inactiveDotStyle={Styles.inactiveDot}
                inactiveDotOpacity={1}
                inactiveDotScale={1}/>
        )
    }
}