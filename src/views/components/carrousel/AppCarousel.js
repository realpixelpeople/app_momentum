import Carousel from 'react-native-snap-carousel';
import React, {Component} from 'react';
import {Platform,  Image , TouchableOpacity, Text, View} from 'react-native';
import AppPagination from './AppPagination';
import { AppText } from '../text'
import { AppWhiteCard } from '../cards'
import Styles from './Styles'
import { ImageCacheManager, CachedImage } from "react-native-cached-image";

export default class AppCarousel extends Component {

    constructor(props) {
        super(props)

        this.state = {
            entries: props.entries
        }
        this._pagination = null
        _navigation = this.props.navigation
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.entries != null && nextProps.entries != this.state.entries) {
            this.setState({ entries: nextProps.entries })
        }
    }

    _renderItem({item, index}) {
        //<Image source={require("../../../img/rivieraStaCristina.png")} style={Styles.image} />
        //  
        return(
            <TouchableOpacity activeOpacity={1} onPress={() => _navigation.navigate("Enterprise", { enterprise: item })}>
                <AppWhiteCard style={Styles.cardContainer}>
                    <CachedImage source={{uri: item.logo}} style={Styles.image} />        
                </AppWhiteCard>               
            </TouchableOpacity>
        )
    }


    render() {
        return(
            <View style={Styles.carousel}>
                <Carousel
                    ref={(comp) => {this.props.refs(comp)}}
                    data={this.state.entries}
                    itemWidth={this.props.itemWidth}
                    sliderWidth={this.props.sliderWidth}
                    renderItem={this._renderItem}
                    containerCustomStyle={Styles.itemContainer}
                    slideStyle={{ backgroundColor: 'transparent' }}
                    onSnapToItem={(index) => { 
                        this.props.onSnapToItem(index)
                        this._pagination.updateActiveSlide(index)}
                    }/>
                <AppPagination
                    ref={(pag) => {this._pagination = pag}}
                    length={this.state.entries.length} />
            </View>
        )
    }
}

