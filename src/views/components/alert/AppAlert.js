import React, {Component} from 'react';
import {Platform,  TouchableOpacity , Text, View} from 'react-native';
import Colors from '../../../utils/AppColors'
import AwesomeAlert from 'react-native-awesome-alerts';

export default class AppAlert extends Component {

    constructor(props) {
        super(props)

        this.state = {
            total: 0,
            received: 0,
            showAlert: true
        }
    }

    updateState(total, received, show) {
        this.setState({ total, received, showAlert: show })
    }

    render() {
        return(
            <AwesomeAlert
                show={this.state.showAlert}
                showProgress={true}
                title="AwesomeAlert"
                message="I have a message for you!"
                progressSize={this.state.total}
                progressColor={Colors.PRIMARY_DARK}
                closeOnTouchOutside={true}
                closeOnHardwareBackPress={false}
                showCancelButton={true}
                showConfirmButton={true}
                cancelText="No, cancel"
                confirmText="Yes, delete it"
                confirmButtonColor="#DD6B55"
                onCancelPressed={() => {
                    this.hideAlert();
                }}
                onConfirmPressed={() => {
                    this.hideAlert();
                }}
                />
        )
    }

}