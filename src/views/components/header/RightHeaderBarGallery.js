
import React, {Component} from 'react';
import {Platform, View, TouchableWithoutFeedback , Text, Image} from 'react-native';

import Styles from './Styles'

export default class RightHeaderBarGallery extends Component {

    render() {
        return(
            <View style={{ display: 'flex', flexDirection: 'row' }}>
            <TouchableWithoutFeedback onPress={this.props.onPressInfo}>
                <Image style={[Styles.gallery, { marginLeft: 20 }]} source={require('../../../img/infoButton.png')} />
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback onPress={this.props.onPressShare}>
                <Image style={[Styles.gallery, { marginLeft: 20 }]} source={require('../../../img/shareButton.png')} />
            </TouchableWithoutFeedback>
            </View>
        )
    }
        
    
}