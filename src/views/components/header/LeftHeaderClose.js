
import React, {Component} from 'react';
import {Platform, View, TouchableWithoutFeedback , Text, Image} from 'react-native';

import Styles from './Styles'

export default class LeftHeaderClose extends Component {

    render() {
        return(
            <View style={{ display: 'flex', flexDirection: 'row' }}>
            <TouchableWithoutFeedback onPress={this.props.onPress}>
                <Image style={[Styles.gallery, { marginLeft: 20 }]} source={require('../../../img/closeButton.png')} />
            </TouchableWithoutFeedback>
            </View>
        )
    }
        
    
}