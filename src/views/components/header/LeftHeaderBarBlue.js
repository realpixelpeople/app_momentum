
import React, {Component} from 'react';
import {Platform,  TouchableWithoutFeedback , Text, Image} from 'react-native';

import Styles from './Styles'

export default class LeftHeaderBarBlue extends Component {

    render() {
        return(
            <TouchableWithoutFeedback onPress={this.props.onPress}>
                <Image style={[Styles.gallery, { marginLeft: 20 }]} source={require('../../../img/backButtonBlue.png')} />
            </TouchableWithoutFeedback>
        )
    }
        
    
}