
import React, {Component} from 'react';
import {Platform,  TouchableWithoutFeedback , Text, Image} from 'react-native';

import Styles from './Styles'

export default class RightHeaderBar extends Component {

    render() {
        return(
            <TouchableWithoutFeedback onPress={this.props.onPress}>
                <Image style={Styles.gallery} source={require('../../../img/gallery.png')} />
            </TouchableWithoutFeedback>
        )
    }
        
    
}