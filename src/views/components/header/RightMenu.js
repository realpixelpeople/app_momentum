
import React, {Component} from 'react';
import {Platform, View, TouchableWithoutFeedback , Text, Image} from 'react-native';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import Styles from './Styles'

export default class RightMenu extends Component {

    constructor(props) {
        super(props)

        this.callback = props.onPress.bind(this)
        this.currentSelected = props.currentSelected
        this.state = {
            currentSelected: this.currentSelected != null ? this.currentSelected : "Todas"
        }
    }

    _menu = null;

    setMenuRef = ref => {
        this._menu = ref;
    };

    hideMenu(item) {
        this._menu.hide();
        this.callback(item)
        this.setState({ currentSelected: item })

    };

    showMenu = () => {
        this._menu.show();
    };
    
    render() {
        return (
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginRight: 20 }}>
            <Menu
              ref={this.setMenuRef}
              button={<TouchableWithoutFeedback onPress={this.showMenu}><Image source={require("../../../img/optionsButton.png")} /></TouchableWithoutFeedback>}
            >
              <MenuItem onPress={this.hideMenu.bind(this, "Todas")} disabled={this.state.currentSelected == "Todas"}>Exibir tudo</MenuItem>
              <MenuItem onPress={this.hideMenu.bind(this, "Videos")} disabled={this.state.currentSelected == "Videos"}>Exibir apenas videos</MenuItem>
              <MenuItem onPress={this.hideMenu.bind(this, "Fotos")} disabled={this.state.currentSelected == "Fotos"}>Exibir apenas fotos</MenuItem>
            </Menu>
          </View>
        );
      }
        
    
}