
import React, {Component} from 'react';
import {Platform,  TouchableWithoutFeedback , Text, Image} from 'react-native';

import Styles from './Styles'

export default class LeftHeaderBar extends Component {

    render() {
        return(
            <TouchableWithoutFeedback onPress={this.props.onPress}>
                <Image style={[Styles.gallery, { marginLeft: 20 }]} source={require('../../../img/backButton.png')} />
            </TouchableWithoutFeedback>
        )
    }
        
    
}