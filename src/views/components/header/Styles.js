
import {StyleSheet} from 'react-native'
import Colors from '../../../utils/AppColors'

const styles = StyleSheet.create({
    content: {
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        flex: 1,
        backgroundColor: 'red'
    },
    logo: {
        flex: 1,
        resizeMode: 'contain'
    },
    gallery: {
        resizeMode: 'contain',
        width: 30,
        height: 30,
        marginRight: 10
    }
})

export default styles