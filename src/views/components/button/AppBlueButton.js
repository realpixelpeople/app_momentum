import React, {Component} from 'react';
import {Platform,  TouchableOpacity , Text, View} from 'react-native';
import Styles from './Styles'
import { AppTextBold } from '../text'
import Colors from '../../../utils/AppColors'

export default class AppBlueButton extends Component {

    render() {
        return(
            <TouchableOpacity onPress={this.props.onPress}>
                <View style={Styles.roundedBlueButton}>
                    <AppTextBold fontSize={this.props.fontSize} fontColor={Colors.WHITE}>{this.props.children}</AppTextBold>
                </View>
            </TouchableOpacity>
        )
    }
}