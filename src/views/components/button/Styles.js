
import {StyleSheet} from 'react-native'
import Colors from '../../../utils/AppColors'

const styles = StyleSheet.create({
    roundedBlueButton: {
        borderRadius: 50,
        backgroundColor: Colors.PRIMARY_DARK,
        paddingLeft: 25,
        paddingRight: 25,
        paddingTop: 10,
        paddingBottom: 10,
        overflow: 'hidden'
    },
    roundedWhiteButton: {
        borderRadius: 50,
        backgroundColor: Colors.WHITE,
        paddingLeft: 25,
        paddingRight: 25,
        paddingTop: 10,
        paddingBottom: 10,
        overflow: 'hidden'
    }
})

export default styles