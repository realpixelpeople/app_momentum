import React, {Component} from 'react';
import {Platform,  TouchableOpacity , Text, View} from 'react-native';
import Styles from './Styles'

export default class AppWhiteCard extends Component {

    render() {
        return(
            <View style={[Styles.roundedWhiteCard, this.props.style]}>
                {this.props.children}
            </View>
        )
    }
}