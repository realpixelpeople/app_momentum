
import {StyleSheet, Dimensions} from 'react-native'
import Colors from '../../../utils/AppColors'
const width = Dimensions.get('window').width ; 
const height = Dimensions.get('window').height ; 

const styles = StyleSheet.create({
    roundedBlueCard: {
        borderRadius: 3,
        backgroundColor: Colors.PRIMARY_DARK,
        paddingLeft: 25,
        paddingRight: 25,
        paddingTop: 10,
        paddingBottom: 10,
        overflow: 'hidden'
    },
    roundedWhiteCard: {
        borderRadius: 10,
        backgroundColor: Colors.WHITE,
        height: height / 5,
        width: width - 20,
        overflow: 'hidden'
    }
})

export default styles