import React, {Component} from 'react';
import {Platform, Image, Dimensions, View} from 'react-native';
import FitImage from 'react-native-fit-image';
import Loader from './Loader'
import SplashController from '../../../controllers/SplashController';
import {StackActions, NavigationActions} from 'react-navigation'; 
import {PermissionsAndroid} from 'react-native';

const width = Dimensions.get('window').width ; 
const height = Dimensions.get('window').height ; 

export default class SplashScreen extends Component {

    static navigationOptions = ({navigation}) => {
        const {params} = navigation.state;

        let MyTransition = (index, position) => {
            const inputRange = [index - 1, index, index + 1];
            const opacity = position.interpolate({
                inputRange,
                outputRange: [.8, 1, 1],
            });
        
            const scaleY = position.interpolate({
                inputRange,
                outputRange: ([0.8, 1, 1]),
            });
        
            return {
                opacity,
                transform: [
                    {scaleY}
                ]
            };
        };

        let TransitionConfiguration = () => {
            return {
                // Define scene interpolation, eq. custom transition
                screenInterpolator: (sceneProps) => {
        
                    const {position, scene} = sceneProps;
                    const {index} = scene;
        
                    return MyTransition(index, position);
                }
            }
        };

        return {
            transitionConfig: TransitionConfiguration,
            headerTransparent: true,
            headerStyle: {
                mode: 'modal'
            },
            headerMode: 'none',
            headerTitle: ""
        }
    }

    async requestCameraPermission() {
        try {
          const granted = await PermissionsAndroid.requestMultiple([
            PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        ],
            {
              title: 'Momentum precisa de acesso a seu armazenamento',
              message:
                'Para que o app funcione perfeitamente e que você possa salvar seus vídeos e fotos' +
                'precisamos de acesso ao armazenamento.',
              buttonNeutral: 'Perguntar depois',
              buttonNegative: 'Cancelar',
              buttonPositive: 'Aceitar',
            },
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            console.log('You can use the camera');
          } else {
            alert("O app pode não funcionar devidamente!")
          }
        } catch (err) {
          console.warn(err);
        }
      }

    constructor(props) {
        super(props)
        this.controller = new SplashController(this.onLoadCards.bind(this), this.onError.bind(this), this.onProgress.bind(this))
        this.controller.callApi()

        this.loader = null
    }

    onLoadCards(x) {

    }

    onError(e) {

    }

    onProgress(pr) {

    }

    resetStack() {
        const resetAction = StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({ routeName: 'Home'})
            ] })
        this.props.navigation.dispatch(resetAction);
    }

    render() {
        return(
            <View style={{ flex: 1, display: 'flex', alignContent: 'center', justifyContent: 'center', alignItems: 'center', flexDirction: 'column' }}>
                <Image style={{ flex: 1, position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, resizeMode: 'cover', width, height }}
                    source={require('../../../img/bg_splash.png')}/>
                <Image style={{ width: "70%", resizeMode: 'contain' }} source={require('../../../img/splash.png')}/>
                <Loader ref={(loader) => { this.loader = loader }} onEnd={() => { this.resetStack() }}/>
            </View>
        )
    }

}