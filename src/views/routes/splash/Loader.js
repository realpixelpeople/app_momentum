import React, {Component} from 'react';
import {Platform, Image, Dimensions, View} from 'react-native';
import * as Progress from 'react-native-progress';

const width = Dimensions.get('window').width ; 
const height = Dimensions.get('window').height ; 


export default class Loader extends Component {

    constructor(props) {
        super(props)
        this.onEnd = props.onEnd.bind(this)
        this.state = {
            progressState: 0
        }
    }

    componentDidMount() {
        this.animate()
    }

    // Faz o loader 
    animate() {
        const progress = this.state.progressState + 0.1
        this.setState({ progressState: progress })
        setTimeout(() => {
            if(this.state.progressState < 1) {
                this.animate()
            } else {
                this.onEnd()
            } 
        }, 350)
    }


    render() {
        const w = width * 0.70
        return(
            <View>
                <Progress.Bar progress={this.state.progressState} unfilledColor="#fff" borderWidth={0} height={1} width={w}/>
            </View>
        )
    }
}