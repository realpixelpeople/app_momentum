import React, {Component} from 'react';
import {TouchableWithoutFeedback, Modal, Image, FlatList, Dimensions, View, ScrollView, TouchableOpacity} from 'react-native';
import APP_COLORS from '../../../utils/AppColors'
import { AppText, AppTextBold } from '../../components/text';

let that = null
export default class FilterType extends Component {


    constructor(props) {
        super(props)
        const items = props.items
        const all = {
            nome: "Todas"
        }

        const newValues = items.filter(value => {
            return value.nome != all.nome
        })

        
        newValues.unshift(all)
        
        this.state = {
            filters: newValues,
            currentSelected: "Todas"
        }
        that = this
    }

    _keyExtractor = (item,index) => item.id

    render() {
        return(
            <View style={{ backgroundColor: '#fff' }}>
                <FlatList
                    data={this.state.filters}
                    extraData={this.state}
                    renderItem={this.renderItem}
                    keyExtractor={this._keyExtractor}
                    numColumns={1}
                    horizontal={true}/>
            </View>
        )
    }

    changeItem(itemName) {
        if(that.state.currentSelected != itemName) {
            that.setState({ currentSelected: itemName })
            that.props.onChangeItem(itemName)
        }
    }

    renderItem({ item }) {
        const color = that.state.currentSelected == item.nome ? APP_COLORS.PRIMARY_DARK : "#fff"
        const textColor = that.state.currentSelected == item.nome ? "#fff" : APP_COLORS.PRIMARY_DARK

        return(
            <TouchableWithoutFeedback onPress={() => {
                that.changeItem(item.nome)
            }}>
                <View style={{ borderWidth: 2, borderRadius: 90, backgroundColor: color, padding: 10, margin: 10, borderColor: APP_COLORS.PRIMARY_DARK }}>
                    <AppTextBold fontColor={textColor}>{item.nome}</AppTextBold>
                </View>
            </TouchableWithoutFeedback>
        )
    }

}