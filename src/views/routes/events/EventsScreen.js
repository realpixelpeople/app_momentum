import React, {Component} from 'react';
import APP_COLORS from '../../../utils/AppColors'
import { AppWhiteCard } from '../../components/cards' 
import  AppAlert from '../../components/alert/AppAlert' 
import {TouchableWithoutFeedback, Platform, Image, FlatList, Dimensions, View, Linking, TouchableOpacity} from 'react-native';
import ImageViewer from 'react-native-image-zoom-viewer';
import Video from 'react-native-video'
import { RightMenu, LeftHeaderBarBlue } from '../../components/header';
import ActionButton from 'react-native-action-button';
import { MomentumDAO } from '../../../infra/internal';
import FilterType from './FilterType';
import { ImageCacheManager, CachedImage } from "react-native-cached-image";
import AwesomeAlert from 'react-native-awesome-alerts';
const width = Dimensions.get('window').width ; 
const height = Dimensions.get('window').height ; 
import FileViewer from 'react-native-file-viewer';

let that = null
export default class EventsScreen extends Component {

    static navigationOptions = ({navigation}) => {
        const {params} = navigation.state;
    
        return {
            headerStyle: {
                backgroundColor: '#fff',
            },
            headerTintColor: APP_COLORS.PRIMARY_DARK,
            headerTitle: "Eventos",
            headerRight: (<RightMenu onPress={(selected) => {
                that.changeByFilter(selected)
            }}/>),
            headerLeft: (<LeftHeaderBarBlue onPress={() => {
                navigation.goBack()
            }}/>)
        }   
    }

    _keyExtractor = (item,index) => item.item.thumb

    constructor(props) {
        super(props)
        const items = props.navigation.getParam("events",[])
        const type = props.navigation.getParam("type","picture")
        this._momentumDAO = new MomentumDAO()

        const all = []
        if(items.categorias != null) {
            items.categorias.forEach(values => {
                const pictures = values.fotos != null ? values.fotos[Object.keys(values.fotos)[0]] : []
                const videos = values.videos != null ? values.videos[Object.keys(values.videos)[0]] : []
                if(pictures != null) {
                    pictures.forEach(picture => {
                        const itemsToAdd = {
                            type: 'fotos',
                            event: values.nome,
                            item: picture
                        }
                      
                        all.push(itemsToAdd)
                    })  
                }
                
                if(videos != null) {
                    videos.forEach(video => {
                        const itemsToAdd = {
                            type: 'videos',
                            event: values.nome,
                            item: video
                        }
                        
                        all.push(itemsToAdd)
                    })  
                }
            })
        }

        const filtered = all.filter(v => {
            return v.item != null && v.item != undefined
        })

        this.state = {
            enterpriseItems: items,
            allItems: filtered,
            data: filtered,
            typeItem: type,
            currentItem: "Todas",
            modalIsVisible: false,
            columnItems: 2
        }
        this.canIntent = false
        that = this
    }

    changeByFilter(type) {
       if(type == "Todas") {
            const values = that.state.currentItem == "Todas" ? that.state.allItems : that.state.allItems.filter(values => {
                return values.event == that.state.currentItem
            })
           that.setState({ data: values })
       } else if(type == "Videos") {
            const all = that.state.currentItem == "Todas" ? that.state.allItems : that.state.allItems.filter(values => {
                return values.event == that.state.currentItem
            })
           const values = all.filter(v => {
               return v.type == "videos"
           })
           that.setState({ data: values })
       } else if(type == "Fotos") {
            const all = that.state.currentItem == "Todas" ? that.state.allItems : that.state.allItems.filter(values => {
                return values.event == that.state.currentItem
            })
            console.log("type", all)
            const values = all.filter(v => {
                return v.type == "fotos"
            })
            that.setState({ data: values })
       }
    }

    render(){
        const icon = that.state.columnItems == 2 ? require("../../../img/eventListIcon.png") :  require("../../../img/galleryButton.png")
        const value = that.state.columnItems == 2 ? 1 : 2
        return(
            <View style={{ flex: 1, backgroundColor: '#fff', flexDirection: 'column' }}>
                <FilterType
                    items={this.state.enterpriseItems.categorias}
                    onChangeItem={(itemName) => {
                        if(itemName == "Todas") {
                            that.setState({ data: that.state.allItems, currentItem: "Todas" })
                        } else {
                            const all = that.state.allItems.filter(values => {
                                return values.event == itemName
                            })
                            that.setState({ data: all, currentItem: itemName })
                        }
                    }}/>

                {this.renderGallery()}
                <ActionButton
                    renderIcon={() => { return(<Image source={icon}/>) }}
                    buttonColor="rgba(255,255,255,1)"
                    onPress={() => { this.setState({ columnItems: value })}}
                >
                </ActionButton>
                <AwesomeAlert
                    show={this.state.showAlert}
                    showProgress={true}
                    title="Realizando o download"
                    message="Aguarde, estamos realizando o download da sua imagem / video"
                    progressSize={this.state.total}
                    progressColor={APP_COLORS.PRIMARY_DARK}
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={true}
                    showCancelButton={true}
                    showConfirmButton={false}
                    cancelText="Cancelar"
                    confirmText="Yes, delete it"
                    confirmButtonColor="#DD6B55"
                    onCancelPressed={() => {
                        this.canIntent = false
                    }}
                    onConfirmPressed={() => {
                        
                    }}
                />
            </View>
        )
    }

    renderGallery() {
        return(
            <View style={{ flex: 1, backgroundColor: APP_COLORS.WHITE, marginBottom: 10 }}>
                <FlatList
                    data={this.state.data}
                    renderItem={this.renderGalleryItem}
                    keyExtractor={this._keyExtractor}
                    key={that.state.columnItems}
                    numColumns={that.state.columnItems}/>
            </View>
        )
    }
    
        renderGalleryItem({ item }) {
            const w = that.state.columnItems == 2 ? width * 0.47 : width * 0.98
            const h = w
            // that.props.navigation.navigate("ImageGallery", { imageUri: item.item.large }) 
            const renderType = item.type == "fotos" ? (
                <View style={{ width: w, height: h, backgroundColor: "#0057AE", borderColor: APP_COLORS.WHITE, borderWidth: 0, margin: 5 }}>
                    <TouchableWithoutFeedback onPress={() => { 
                            that.setState({ showAlert: true })
                            that.canIntent = true
                            that._momentumDAO.storeImages(item.item.large, "external", (path) => {
                                const p = Platform.OS === 'android' ? '' + path : '' + path
                                if(that.canIntent) {
                                    FileViewer.open(p).then(s => console.log('success', s)).catch(e => alert("Não foi possivel abrir esse arquivo"))
                                }
                                that.setState({ showAlert: false })  
                            })
                        }}>
                        
                        <CachedImage style={{ width: w, height: h, resizeMode: 'stretch' }} source={{ uri: item.item.thumb }}/>
                    </TouchableWithoutFeedback>
                </View>
            ) : (
                <View style={{ width: w, height: h, backgroundColor: "#0057AE", borderColor: APP_COLORS.WHITE, borderWidth: 0, margin: 5 }}>
                    <TouchableWithoutFeedback onPress={() => { 
                        that.setState({ showAlert: true })
                        that.canIntent = true
                        that._momentumDAO.storeImages(item.item.large, "external", (path) => {
                            const p = Platform.OS === 'android' ? '' + path : '' + path
                            if(that.canIntent) {
                                FileViewer.open(p).then(s => console.log('success', s)).catch(e => alert("Não foi possivel abrir esse arquivo"))
                            }
                            that.setState({ showAlert: false })  
                        })
                     }}>
                        <View>
                        <CachedImage style={{ width: w, height: h, resizeMode: 'stretch' }} source={{ uri: item.item.thumb }}/>
                        <Image style={{ position: 'absolute', top: 0, left: 0, bottom: 0, right: 0, width: w, height: h, resizeMode: 'stretch'}} source={require("../../../img/playButton.png")}/>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            )
    
            return renderType
        }
    }

