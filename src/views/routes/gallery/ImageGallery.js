import {TouchableWithoutFeedback, Modal, Image, Share, Dimensions, View} from 'react-native';
import {RightHeaderBarGallery, LeftHeaderBar} from '../../components/header'
import React, {Component} from 'react';
import { AppWhiteCard } from '../../components/cards';
import { AppText } from '../../components/text';
import APP_COLORS from '../../../utils/AppColors';

const width = Dimensions.get('window').width ; 
const height = Dimensions.get('window').height ; 

export default class ImageGallery extends Component {

    static navigationOptions = ({navigation}) => {
        const {params} = navigation.state;

        return {
            headerTransparent: true,
            headerStyle: {
                borderBottomWidth: 0,
                backgroundColor: APP_COLORS.PRIMARY_DARK,
            },
            headerTitle: "",
            headerLeft: (<LeftHeaderBar onPress={() => {
                navigation.goBack()
            }}/>),
            headerRight: (<RightHeaderBarGallery onPressInfo={() => {
                that.setState({ modalIsVisible: true })
            }} onPressShare={() => {
                that.onShare()
            }}/>)
        }
    }

    constructor(props) {
        super(props)
        const imageUri = props.navigation.getParam("imageUri", "")
        this.state = {
            image: imageUri,
            modalIsVisible: false
        }
        that = this
    }

    async onShare() {
        try {
          const result = await Share.share({
            message:
              'Momentum',
          })
    
          if (result.action === Share.sharedAction) {
            if (result.activityType) {
              // shared with activity type of result.activityType
            } else {
              // shared
            }
          } else if (result.action === Share.dismissedAction) {
            // dismissed
          }
        } catch (error) {
        }
      };

    render() {
        const w = width
        const h = height 

        return(
            <View style={{ flex: 1, backgroundColor: '#0058AC', justifyContent: 'center', alignItems: 'center', alignSelf: 'center' }}>
                <Image style={{ alignSelf: 'stretch', width, height: h,  resizeMode: "cover" }} source={{ uri: this.state.image }} />
                {this.renderModal()}
            </View>
        )
    }

    renderModal() {
        const w = width * 0.70
        const h = height / 4
        return(
            <Modal visible={this.state.modalIsVisible} transparent={true} onRequestClose={() => {  that.setState({ modalIsVisible: false }) }}>
                <View style={{ flex: 1, display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', alignSelf: 'center'}}>
                    <AppWhiteCard style={{ height: h, width: w,display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', alignSelf: 'center'}}>
                            <View style={{ position: 'absolute', top: 0, left: 0, margin: 15 }}>
                                <TouchableWithoutFeedback onPress={() => { that.setState({ modalIsVisible: false })}}>
                                    <Image source={require("../../../img/closeButton.png")}/>
                                </TouchableWithoutFeedback>
                            </View>

                            <Image source={require("../../../img/infoBlue.png")}/>
                            <AppText>Momentum</AppText>
                    </AppWhiteCard>
                </View>
            </Modal>
        )
    }
}