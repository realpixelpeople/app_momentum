import React, {Component} from 'react';
import APP_COLORS from '../../../utils/AppColors'
import { AppWhiteCard } from '../../components/cards' 
import {TouchableWithoutFeedback, Platform, Image, FlatList, Dimensions, View} from 'react-native';
import ImageViewer from 'react-native-image-zoom-viewer';
import Video from 'react-native-video'
import { RightMenu, LeftHeaderBarBlue } from '../../components/header';
import ActionButton from 'react-native-action-button';
import { MomentumDAO } from '../../../infra/internal';
const width = Dimensions.get('window').width ; 
const height = Dimensions.get('window').height ; 
import FileViewer from 'react-native-file-viewer';
let that = null
import AwesomeAlert from 'react-native-awesome-alerts';
import { ImageCacheManager, CachedImage } from "react-native-cached-image";

export default class GalleryScreen extends Component {

    static navigationOptions = ({navigation}) => {
        const {params} = navigation.state;

        return {
            headerStyle: {
                backgroundColor: "#fff",
              },
            headerTintColor: APP_COLORS.PRIMARY_DARK,
            headerTitle: `${navigation.state.params.title}`,
            // headerRight: (<RightMenu onPress={(type) => { 
            //     that.filterValues(type)
            // }}/>),
            headerLeft: (<LeftHeaderBarBlue onPress={() => {
                navigation.goBack()
            }}/>)
        }
    }

    _keyExtractor = (item, index) => item.id;

    filterValues(type) {
        console.log(type)
    }

    constructor(props) {
        super(props)
        const items = props.navigation.getParam("items", [])
        const type = props.navigation.getParam("type", "picture")
        this._momentumDAO = new MomentumDAO()

    
        this.state = {
            data: items[Object.keys(items)[0]],
            typeItem: type,
            currentItem: null,
            modalIsVisible: false,
            headerTitle: type == "picture" ? "Fotos" : type == "tour" ? "Tour 360°" : "Vídeos",
            columnItems: 2,
            showAlert: false
        } 

        this.canIntent = true
        that = this

    }

    render() {
        const icon = that.state.columnItems == 2 ? require("../../../img/eventListIcon.png") :  require("../../../img/galleryButton.png")
        const value = that.state.columnItems == 2 ? 1 : 2
        return(
            <View style={{ flex: 1, backgroundColor: '#fff' }}>
                {this.renderGallery()}
                <ActionButton
                    renderIcon={() => { return(<Image source={icon}/>) }}
                    buttonColor="rgba(255,255,255,1)"
                    onPress={() => { this.setState({ columnItems: value })}}
                >
                    
                </ActionButton>
                <AwesomeAlert
                    show={this.state.showAlert}
                    showProgress={true}
                    title="Realizando o download"
                    message="Aguarde, estamos realizando o download da sua imagem / vídeo"
                    progressSize={this.state.total}
                    progressColor={APP_COLORS.PRIMARY_DARK}
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={true}
                    showCancelButton={true}
                    showConfirmButton={false}
                    cancelText="Cancelar"
                    confirmText="Yes, delete it"
                    confirmButtonColor="#DD6B55"
                    onCancelPressed={() => {
                        this.canIntent = false
                    }}
                    onConfirmPressed={() => {
                        
                    }}/>
            </View>
        )
    }

    renderFab() {

    }

    renderGallery() {
        return(
            <View style={{ flex: 1, backgroundColor: APP_COLORS.WHITE, marginBottom: 10 }}>
                <FlatList
                    data={this.state.data}
                    renderItem={this.renderGalleryItem}
                    keyExtractor={this._keyExtractor}
                    key={that.state.columnItems}
                    numColumns={that.state.columnItems}/>
            </View>
        )
    }

    renderGalleryItem({ item }) {
        const w = that.state.columnItems == 2 ? width * 0.47 : width * 0.98
        const h = w

        const renderType = that.state.typeItem == "picture" ? (
            <View style={{ width: w, height: h, backgroundColor: "#0057AE", borderColor: APP_COLORS.WHITE, borderWidth: 0, margin: 5 }}>
                <TouchableWithoutFeedback onPress={() => { 
                    that.setState({ showAlert: true })
                    that.canIntent = true
                    that._momentumDAO.storeImages(item.large, "external", (path) => {
                        const p = Platform.OS === 'android' ? '' + path : '' + path
                        if(that.canIntent) {
                            FileViewer.open(p).then(s => console.log('success', s)).catch(e => alert("Não foi possivel abrir esse arquivo"))
                        }
                        that.setState({ showAlert: false })  
                    })
                 }}>
                    <CachedImage style={{ width: w, height: h, resizeMode: 'stretch' }} source={{ uri: item.thumb }}/>
                </TouchableWithoutFeedback>
            </View>
        ) :  that.state.typeItem == "tour" ? (
            <View style={{ width: w, height: h, backgroundColor: "#0057AE", borderColor: APP_COLORS.WHITE, borderWidth: 0, margin: 5 }}>
                <TouchableWithoutFeedback onPress={() => { 
                    that.setState({ showAlert: true })
                    that.canIntent = true
                    that._momentumDAO.storeImages(item.large, "external", (path) => {
                        const p = Platform.OS === 'android' ? '' + path : '' + path
                        if(that.canIntent) {
                            FileViewer.open(p).then(s => console.log('success', s)).catch(e => alert("Não foi possivel abrir esse arquivo"))
                        }
                        that.setState({ showAlert: false })  
                    })
                 }}>
                    <CachedImage style={{ width: w, height: h, resizeMode: 'stretch' }} source={{ uri: item.thumb }}/>
                </TouchableWithoutFeedback>
            </View>
        ) : (
            <View style={{ width: w, height: h, backgroundColor: "#0057AE", borderColor: APP_COLORS.WHITE, borderWidth: 0, margin: 5 }}>
                <TouchableWithoutFeedback onPress={() => { 
                    that.setState({ showAlert: true })
                    that.canIntent = true
                    that._momentumDAO.storeVideo(item.large, "external", (path) => {
                        const p = Platform.OS === 'android' ? '' + path : '' + path
                        if(that.canIntent) {
                            FileViewer.open(p).then(s => console.log('success', s)).catch(e => alert("Não foi possivel abrir esse arquivo"))
                        } 
                        that.setState({ showAlert: false })  
                    })
                 }}>
                    <View>
                    <CachedImage style={{ width: w, height: h, resizeMode: 'stretch' }} source={{ uri: item.thumb }}/>
                    <Image style={{ position: 'absolute', top: 0, left: 0, bottom: 0, right: 0, width: w, height: h, resizeMode: 'stretch'}} source={require("../../../img/playButton.png")}/>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        )

        return renderType
    }

}