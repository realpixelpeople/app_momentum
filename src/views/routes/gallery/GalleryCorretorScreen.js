import React, {Component} from 'react';
import APP_COLORS from '../../../utils/AppColors'
import { AppWhiteCard } from '../../components/cards' 
import {TouchableOpacity, Platform, Modal, Image, FlatList, Dimensions, View, Linking} from 'react-native';
import { RightMenu, LeftHeaderBarBlue, RightHeaderTrash, LeftHeaderClose } from '../../components/header';
import ActionButton from 'react-native-action-button';
import ImagePicker from 'react-native-image-picker'
import { CachedImage } from 'react-native-cached-image'
import { MomentumDAO } from '../../../infra/internal';
import { AppTextBold, AppText } from '../../components/text';
import FileViewer from 'react-native-file-viewer';

const width = Dimensions.get('window').width ; 
const height = Dimensions.get('window').height ; 

let that = null
export default class GalleryCorretorScreen extends Component {


    constructor(props) {
        super(props)
        
        
        this.momentumDAO = new MomentumDAO()
        
        this.state = {
            items: [],
            newFilteredItems: [],
            selectedMode: false,
            columnItems: 2,
            selecteds: [],
            modalVisible: false
        }
        
        this.items = []
        that = this
    }



    componentDidMount() {
        this.momentumDAO.getCorretorGallery((values) => {
            console.log('values', values)
            this.setState({ items: values, newFilteredItems: values })   
        })
    }


    _keyExtractor = (item, index) => item.id;

    static navigationOptions = ({navigation}) => {
        const {params} = navigation.state;

        const title = params != null && params.selectedMode != null && params.selectedMode ? params.title : "Galeria do Corretor"
        const right = params != null && params.selectedMode != null && params.selectedMode ? (<RightHeaderTrash onPress={() => {
            that.removeItemsSelecteds()
        }}/>) : (<View></View>)

        const left = params != null && params.selectedMode != null && params.selectedMode  ? (<LeftHeaderClose onPress={() => {
            that.setState({ selectedMode: false })
        }}/>) : (<LeftHeaderBarBlue onPress={() => {
            navigation.goBack()
        }}/>)

        return {
            headerStyle: {
                backgroundColor: "#fff",
              },
            headerTintColor: APP_COLORS.PRIMARY_DARK,
            headerTitle: title,
            headerRight: (<RightMenu onPress={(selected) => {
                that.filterValues(selected)
            }}/>),
            headerLeft:  that && that.state && that.state.selecteds.length > 0 ? right : left
        }
    }

    filterValues(type) {
        let newArray = []

        if(that.state.items.length === 0)
            return
        
        switch(type.toLowerCase()) {
            case 'videos':        
                for(let i=0; i < that.state.items.length; i++) {
                    if(that.state.items[i].type == type.toLowerCase().substring(0, type.length - 1))
                        newArray.push(that.state.items[i])
                }
                break
            case 'fotos':
                for(let i=0; i < that.state.items.length; i++) {
                    if(that.state.items[i].type == type.toLowerCase())
                        newArray.push(that.state.items[i])
                }
                break
            default:
                return this.setState({ newFilteredItems: this.state.items })
        }
        

        this.setState({ newFilteredItems: newArray })
    }

    componentDidUpdate(prevProps, prevState) {
        if(that.state.selectedMode != prevState.selectedMode) {
            const title = that.state.selectedMode ? "Selecione..." :  "Galeria do Corretor"
            that.props.navigation.setParams({
                title:`${title}`,
                selectedMode: that.state.selectedMode
            });   
            if(!that.state.selectedMode) {
                const newItems = that.state.items.map(i => {
                    i.isSelected = false
                    return i
                })
                that.setState({ selecteds: [], items: newItems })
            }
        }

        if(that.state.selecteds.length != prevState.selecteds.length) {
            const title = that.state.selecteds.length == 0 ? that.state.selectedMode ? "Selecione..." : "Galeria do Corretor" : that.state.selecteds.length
            that.props.navigation.setParams({
                title: `${title}`
            }); 
        } 
    }

    removeItemsSelecteds() {
        that.momentumDAO.deleteLocalValues(that.state.selecteds)
        const newItems = that.state.items.filter(i => {
            return that.state.selecteds.indexOf(i) === -1
        })
        that.momentumDAO.storeLocalFiles(newItems)
        that.setState({ items: newItems, newFilteredItems: newItems ,selecteds: [], selectedMode: false })
    }
    

    render() {
        const icon = that.state.columnItems == 2 ? require("../../../img/eventListIcon.png") :  require("../../../img/galleryButton.png")
        const value = that.state.columnItems == 2 ? 1 : 2
        
        return(
            <View style={{ flex: 1, backgroundColor: '#fff' }}>
                {this.renderGallery()}

                
                <ActionButton buttonColor={APP_COLORS.PRIMARY_DARK}>
                    <ActionButton.Item
                        buttonColor="rgba(255,255,255,1)"
                        onPress={() => {
                              this.setState({ modalVisible: true })                       
                        }}>
                        <Image source={require("../../../img/plusButton.png")}/>
                        </ActionButton.Item>
                <ActionButton.Item
                    buttonColor="rgba(255,255,255,1)"
                    onPress={() => { this.setState({ columnItems: value })}}>
                    <Image source={icon}/>
                    </ActionButton.Item>
                </ActionButton>
                
              
                {this.renderModal()}
            </View>
        )
    }

    renderGallery() {
        return(
            <View style={{top: 0, bottom: 0, right: 0, left: 0, flex: 1, backgroundColor: APP_COLORS.WHITE, marginBottom: 10 }}>
                <FlatList
                    data={this.state.newFilteredItems}
                    extraData={this.state}
                    renderItem={this.renderGalleryItem}
                    keyExtractor={this._keyExtractor}
                    key={this.state.columnItems}
                    numColumns={this.state.columnItems}/>
            </View>
        )
    }

    openImage(options, option) {
        ImagePicker.launchImageLibrary(options, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else { 
                const uri = (Platform.OS==='android') ? response.uri : response.uri.replace('file://', '')
                that.momentumDAO.storeLocal(uri, option, "internal", (path) => {
                    const source = { 
                        uri: (Platform.OS==='android') ? path : path.replace('file://', ''),
                        type: option
                    };
    
                    // You can also display the image using data:
                    // const source = { uri: 'data:image/jpeg;base64,' + response.data };
                    const values = that.state.items
                    values.push(source)

                    this.setState({
                        items: values,
                        columnItems: 2,
                        modalVisible: false
                    });
                    
                    that.momentumDAO.storeLocalFiles(values, () => {
                    })
                })                
            }
        });  
    }

    renderGalleryItem({ item }) {
        const renderItem = that.state.selectedMode ? item.isSelected ? that.renderItemSelected(item) : that.renderItemUnselected(item) : that.renderItem(item)
        return renderItem
    }

    renderItemSelected(item) {
        const w = that.state.columnItems == 2 ? width * 0.47 : width * 0.98
        const h = w
        
        const contentW = that.state.columnItems == 2 ? width * 0.40 : width * 0.91
        const contenth = contentW

        const uri = (Platform.OS==='android') ? `file://${item.uri}` : item.uri

        const renderType = item.type == "fotos" ? (
            <View style={{ width: w, height: h, backgroundColor: "#0057AE", alignSelf: 'center', justifyContent: 'center', alignItems: 'center', borderColor: APP_COLORS.WHITE, borderWidth: 0, margin: 5 }}>
                <TouchableOpacity activeOpacity = {1} onPress={() => { 
                       const values = that.state.selecteds.filter(v => {
                            return v.uri != item.uri
                        })

                        const newItems = that.state.items.map(i => {
                            if(i.uri == item.uri) i.isSelected = false
                            return i
                        })
                        console.log("values", values)
                        that.setState({ selecteds: values, item, items: newItems })
                    }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center', alignSelf: 'center' }}>
                        <Image style={{ width: contentW, height: contenth, resizeMode: 'cover', alignSelf: 'center' }} source={{ uri }}/>
                        <Image style={{ position: 'absolute', top: -10, left: -10, bottom: 0, resizeMode: 'cover'}} source={require("../../../img/selectedButton.png")}/>
                    </View>
                </TouchableOpacity>
            </View>
        ) :  item.type == "tour" ? (
            <View style={{ width: w, height: h, backgroundColor: "#0057AE", borderColor: APP_COLORS.WHITE, borderWidth: 0, margin: 5 }}>
                <TouchableOpacity activeOpacity = {1} onPress={() => { 

                    const values = that.state.selecteds.filter(v => {
                        return v.uri != item.uri
                    })

                    const newItems = that.state.items.map(i => {
                        if(i.uri == item.uri) i.isSelected = false
                        return i
                    })

                    that.setState({ selecteds: values, item, items: newItems })
                }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center', alignSelf: 'center' }}>
                        <Image style={{  width: contentW, height: contenth, resizeMode: 'cover', alignSelf: 'center' }} source={{ uri }}/>
                        <Image style={{ position: 'absolute', top: -10, left: -10, bottom: 0, resizeMode: 'cover'}} source={require("../../../img/selectedButton.png")}/>
                    </View>
                </TouchableOpacity>
            </View>
        ) : (
            <View style={{ width: w, height: h, backgroundColor: "#0057AE",alignSelf: 'center', justifyContent: 'center', alignItems: 'center', borderColor: APP_COLORS.WHITE, borderWidth: 0, margin: 5 }}>
                <TouchableOpacity activeOpacity = {1} onPress={() => {
                    const values = that.state.selecteds.filter(v => {
                        return v.uri != item.uri
                    })

                    const newItems = that.state.items.map(i => {
                        if(i.uri == item.uri) i.isSelected = false
                        return i
                    })

                    that.setState({ selecteds: values, item, items: newItems })
                }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center', alignSelf: 'center' }}>
                        <Image style={{ width: contentW, height: contenth, resizeMode: 'cover' }} source={{ uri }}/>
                        <Image style={{ position: 'absolute', top: 0, left: 0, bottom: 0, right: 0, width: contentW, height: contenth, resizeMode: 'cover'}} source={require("../../../img/playButton.png")}/>
                        <Image style={{ position: 'absolute', top: -10, left: -10, bottom: 0, resizeMode: 'cover'}} source={require("../../../img/selectedButton.png")}/>
                    </View>
                </TouchableOpacity>
            </View>
        )

        return renderType
    
    }

    renderItemUnselected(item) {
        const w = that.state.columnItems == 2 ? width * 0.47 : width * 0.98
        const h = w
        
        const uri = (Platform.OS==='android') ? `file://${item.uri}` : item.uri

        const renderType = item.type == "fotos" ? (
            <View style={{ width: w, height: h, backgroundColor: "#0057AE", borderColor: APP_COLORS.WHITE, borderWidth: 0, margin: 5 }}>
                <TouchableOpacity activeOpacity = {1} onPress={() => {
                        const values = that.state.selecteds.slice()
                        values.push(item)
                        const newItems = that.state.items.map(i => {
                            if(i.uri == item.uri) i.isSelected = true
                            return i
                        })
    
                        that.setState({ selecteds: values, item, items: newItems })
                    }}>
                    <View>
                        <Image style={{ width: w, height: h, resizeMode: 'cover' }} source={{ uri }}/>
                        <Image style={{ position: 'absolute', top: 5, left: 5, bottom: 0, resizeMode: 'cover'}} source={require("../../../img/selectButton.png")}/>
                    </View>
                </TouchableOpacity>
            </View>
        ) :  item.type == "tour" ? (
            <View style={{ width: w, height: h, backgroundColor: "#0057AE", borderColor: APP_COLORS.WHITE, borderWidth: 0, margin: 5 }}>
                <TouchableOpacity activeOpacity = {1} onPress={() => { 
                     const values = that.state.selecteds.slice()
                     values.push(item)
                     const newItems = that.state.items.map(i => {
                         if(i.uri == item.uri) i.isSelected = true
                         return i
                     })
 
                     that.setState({ selecteds: values, item, items: newItems })
                }}>
                    <View>
                    <Image style={{ width: w, height: h, resizeMode: 'cover' }} source={{ uri }}/>
                    <Image style={{ position: 'absolute', top: 0, left: 0, bottom: 0, resizeMode: 'cover'}} source={require("../../../img/selectButton.png")}/>
                    </View>
                </TouchableOpacity>
            </View>
        ) : (
            <View style={{ width: w, height: h, backgroundColor: "#0057AE", borderColor: APP_COLORS.WHITE, borderWidth: 0, margin: 5 }}>
                <TouchableOpacity  activeOpacity = {1} onPress={() => {
                     const values = that.state.selecteds.slice()
                     values.push(item)
                     const newItems = that.state.items.map(i => {
                         if(i.uri == item.uri) i.isSelected = true
                         return i
                     })
 
                     that.setState({ selecteds: values, item, items: newItems })
                }}>
                    <View>
                        <Image style={{ width: w, height: h, resizeMode: 'cover' }} source={{ uri }}/>
                        <Image style={{ position: 'absolute', top: 0, left: 0, bottom: 0, right: 0, width: w, height: h, resizeMode: 'cover'}} source={require("../../../img/playButton.png")}/>
                        <Image style={{ position: 'absolute', top: 5, left: 5, bottom: 0, resizeMode: 'cover'}} source={require("../../../img/selectButton.png")}/>
                    </View>
                </TouchableOpacity>
            </View>
        )

        return renderType
    }

    renderItem(item) {
        const w = that.state.columnItems == 2 ? width * 0.47 : width * 0.98
        const h = w

        const uri = (Platform.OS==='android') ? `file://${item.uri}` : item.uri

        const renderType = item.type == "fotos" ? (
            <View style={{ width: w, height: h, backgroundColor: "#0057AE", borderColor: APP_COLORS.WHITE, borderWidth: 0, margin: 5 }}>
                <TouchableOpacity activeOpacity = {1}  rejectResponderTermination onPress={() => { 
                    console.log("item.uri",item.uri)
                      FileViewer.open(item.uri).then(s => console.log('success', s)).catch(e => alert("Não foi possivel abrir o arquivo"))
                    }}
                    onLongPress={() => { 
                        that.setState({ selectedMode: true, selecteds: [] })
                    }}>
                    <View>
                        <Image style={{ width: w, height: h, resizeMode: 'cover' }} source={{ uri }}/>
                    </View>
                    
                </TouchableOpacity>
            </View>
        ) :  item.type == "tour" ? (
            <View style={{ width: w, height: h, backgroundColor: "#0057AE", borderColor: APP_COLORS.WHITE, borderWidth: 0, margin: 5 }}>
                <TouchableOpacity activeOpacity = {1} onPress={() => { that.props.navigation.navigate("TourGallery", { imageUri: item.uri }) }}>
                    <Image style={{ width: w, height: h, resizeMode: 'cover' }} source={{ uri }}/>
                </TouchableOpacity>
            </View>
        ) : (
            <View style={{ width: w, height: h, backgroundColor: "#0057AE", borderColor: APP_COLORS.WHITE, borderWidth: 0, margin: 5 }}>
                <TouchableOpacity activeOpacity = {1} onPress={() => {
                    FileViewer.open(item.uri).then(s => console.log('success', s)).catch(e => alert("Não foi possivel abrir o arquivo"))
                }} onLongPress={() => { 
                    that.setState({ selectedMode: true, selecteds: [] })
                }}>
                    <View>
                    <   Image style={{ width: w, height: h, resizeMode: 'cover' }} source={{ uri }}/>
                        <Image style={{ position: 'absolute', top: 0, left: 0, bottom: 0, right: 0, width: w, height: h, resizeMode: 'cover'}} source={require("../../../img/playButton.png")}/>
                    </View>
                </TouchableOpacity>
            </View>
        )

        return renderType
    }

    renderModal() {
        const w = width * 0.70
        const h = height * 0.20
        return(
            <Modal visible={this.state.modalVisible} transparent={true} onRequestClose={() => {  that.setState({ modalVisible: false }) }}>
                <View style={{ flex: 1, display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', alignSelf: 'center'}}>
                    <AppWhiteCard style={{  backgroundColor: "#0057AE", height: h, width: w,display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', alignSelf: 'center'}}>
                            <View style={{ position: 'absolute', top: 0, left: 0, margin: 15 }}>
                                <TouchableOpacity activeOpacity = {1} onPress={() => { that.setState({ modalVisible: false })}}>
                                    <Image source={require("../../../img/closeWhite.png")}/>
                                </TouchableOpacity>
                            </View>

                            <AppTextBold fontSize={20} fontColor={APP_COLORS.WHITE}>Selecionar</AppTextBold>
                            <View style={{display:'flex', flexDirection:'row'}}>
                            <TouchableOpacity activeOpacity = {1} onPress={() => {
                                const options = {
                                    title: 'Selecione a imagem',
                                    mediaType: 'photo',
                                    storageOptions: {
                                        skipBackup: true,
                                        path: 'images',
                                        cameraRoll: true, 
                                        waitUntilSaved: true
                                    }
                                };
                                that.openImage(options, "fotos")
                            }}>
                                 <View style={{ margin: 10, padding: 10 }}>
                                    <Image style={{ width: 50, height: 50, resizeMode: "contain"}} source={require("../../../img/baseline_photo_camera_white_18dp.png")}/>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity activeOpacity = {1} onPress={() => {
                                const options = {
                                    title: 'Selecione a imagem',
                                    mediaType: 'video',
                                    storageOptions: {
                                        skipBackup: true,
                                        path: 'images',
                                        cameraRoll: true, 
                                        waitUntilSaved: true
                                    }
                                };
                                that.openImage(options, "video")
                            }}>
                                <View style={{ margin: 10, padding: 10 }}>
                                    <Image style={{ width: 50, height: 50, resizeMode: "contain"}} source={require("../../../img/baseline_videocam_white_18dp.png")}/>
                                </View>
                            </TouchableOpacity>
                            </View>
                    </AppWhiteCard>
                </View>
            </Modal>
        )
    }
}