import {TouchableWithoutFeedback, Modal, Image, Share, Dimensions, View} from 'react-native';
import {RightHeaderBarTour, LeftHeaderBarTour} from '../../components/header'
import React, {Component} from 'react';
import { WebView } from "react-native-webview";
import AwesomeAlert from 'react-native-awesome-alerts';
import {ImageCachedManager, CachedImage } from 'react-native-cached-image';


const width = Dimensions.get('window').width ; 
const height = Dimensions.get('window').height ; 

const imageWidth = 8 * width;
const imageHeight = 100 *height;


//const middleOfTheScreenX = (imageWidth - width) / 2;

export default class TourGallery extends Component {

    static navigationOptions = ({navigation}) => {
        const {params} = navigation.state;

        return {
            headerTransparent: true,
            headerStyle: {
                borderBottomWidth: 0
            },
            headerTitle: "",
            headerLeft: (<LeftHeaderBarTour onPress={() => {
                navigation.goBack()
            }}/>),
            headerRight: (<View></View>)
        }
    }

    constructor(props) {
        super(props)
    
        const imageUri = props.navigation.getParam("imageUri", "")

        console.log(imageUri)
        this.state = {
            image: imageUri,
            modalIsVisible: true,
            y: 0
        }
        that = this
    }

    async onShare() {
        try {
          const result = await Share.share({
            message:
              'Momentum',
          })
    
          if (result.action === Share.sharedAction) {
            if (result.activityType) {
              // shared with activity type of result.activityType
            } else {
              // shared
            }
          } else if (result.action === Share.dismissedAction) {
            // dismissed
          }
        } catch (error) {
        }
      };

    render() {
        return(
            
                <WebView
                source={{uri: this.state.image}}
                domStorageEnabled={true}
                startInLoadingState={true}
                onError={(err) => console.log('err', err)}
                renderError={(err) => console.log('not', err)}
                scalesPageToFit={true}


                style={{ width, height:700, backgroundColor: '#FFFF', justifyContent: 'center', alignItems: 'center', alignSelf: 'center' }}
            />
            
        )
    }
}