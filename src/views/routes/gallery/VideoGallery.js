import {TouchableWithoutFeedback, Modal, Image, Share, Dimensions, View} from 'react-native';
import {RightHeaderBarGallery, LeftHeaderBar} from '../../components/header'
import React, {Component} from 'react';
import { AppWhiteCard } from '../../components/cards';
import { AppText } from '../../components/text';
import Video from 'react-native-video'
const width = Dimensions.get('window').width ; 
const height = Dimensions.get('window').height ; 


let that = null;


export default class VideoGallery extends Component {

    static navigationOptions = ({navigation}) => {
        const {params} = navigation.state;

        return {
            headerTransparent: true,
            headerStyle: {
                borderBottomWidth: 0
            },
            headerTitle: "",
            headerLeft: (<LeftHeaderBar onPress={() => {
                navigation.goBack()
            }}/>),
            headerRight: (<RightHeaderBarGallery onPressInfo={() => {
                that.setState({ modalIsVisible: true })
            }} onPressShare={() => {
                that.onShare()
            }}/>)
        }
    }

    constructor(props) {
        super(props)
        const videoUri = props.navigation.getParam("videoUri", "")
        this.state = {
            video: videoUri,
            modalIsVisible: false
        }
        that = this
    }

    async onShare() {
        try {
          const result = await Share.share({
            message:
              'Momentum',
          })
    
          if (result.action === Share.sharedAction) {
            if (result.activityType) {
              // shared with activity type of result.activityType
            } else {
              // shared
            }
          } else if (result.action === Share.dismissedAction) {
            // dismissed
          }
        } catch (error) {
        }
      };

    render() {
        const w = width
        const h = height / 2
        
        console.log("Render",that.state.video);

        return(
            <View style={{ flex: 1, backgroundColor: '#0058AC', justifyContent: 'center', alignItems: 'center', alignSelf: 'center' }}>
                <View  style={{ width, height: h }}>
                <Video source={{ uri: that.state.video }}   // Can be a URL or a local file.
                    ref={(ref) => {
                        this.player = ref
                    }}                                      // Store reference
                    onBuffer={this.onBuffer}                // Callback when remote video is buffering
                    onError={this.videoError}
                    controls               // Callback when video cannot be loaded
                    style={{ width, height: h, backgroundColor: 'black' }} />
                </View>
                {this.renderModal()}
            </View>
        )
    }

    renderModal() {
        const w = width * 0.70
        const h = height / 4
        return(
            <Modal visible={this.state.modalIsVisible} transparent={true} onRequestClose={() => {  that.setState({ modalIsVisible: false }) }}>
                <View style={{ flex: 1, display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', alignSelf: 'center'}}>
                    <AppWhiteCard style={{ height: h, width: w,display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', alignSelf: 'center'}}>
                            <View style={{ position: 'absolute', top: 0, left: 0, margin: 15 }}>
                                <TouchableWithoutFeedback onPress={() => { that.setState({ modalIsVisible: false })}}>
                                    <Image source={require("../../../img/closeButton.png")}/>
                                </TouchableWithoutFeedback>
                            </View>

                            <Image source={require("../../../img/infoBlue.png")}/>
                            <AppText>Momentum</AppText>
                    </AppWhiteCard>
                </View>
            </Modal>
        )
    }
}
