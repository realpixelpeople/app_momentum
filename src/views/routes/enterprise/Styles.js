import { StyleSheet, Dimensions } from 'react-native';
const width = Dimensions.get('window').width ; 
const height = Dimensions.get('window').height ; 


const style = StyleSheet.create({
    containerBg: {
        flex: 1
    },  
    top: {
        width,
        height: height / 6,
        top: 0,
        backgroundColor: '#fff',
        alignSelf: 'center',
        alignItems: 'center'
    },
    cardTop: {
        width,
        height: height / 6,
        padding: 10,
        backgroundColor: '#fff',
        alignSelf: 'center',
        alignItems: 'center'
    },
    imageTop: {
        width,
        height: height / 6,
        marginLeft: 10,
        marginRight: 10,
        resizeMode: 'contain',
        justifyContent: 'center'
    },
    listContainer: {
        height: width * 0.45, 
        width: width * 0.45, 
        borderRadius: 10, 
        margin: 10,
        marginTop: 30,
        marginBottom: 30
    },
    listBg: {
        height: width * 0.45, 
        width: width * 0.45, 
        borderRadius: 10
    },
    listCard: {
        position: 'absolute', 
        justifyContent: 'center', 
        alignSelf: 'center', 
        alignItems: 'center', 
        bottom: 0, 
        width: width * 0.45, 
        height: 80, 
        borderRadius: 10, 
        borderBottomColor: 10, 
        marginTop: 50
    },
    listText: {
        justifyContent: 'center', 
        alignSelf: 'center', 
        alignItems: 'center',
        overflow: 'scroll',
        textAlign: 'center',
        marginLeft: 5,
        marginRight: 5
    }
})


export default style