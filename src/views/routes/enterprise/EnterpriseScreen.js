import React, {Component} from 'react';
import {Platform, Image, Dimensions, View, FlatList} from 'react-native';
import Styles from './Styles'
import {HeaderBar, RightHeaderBar, LeftHeaderBar } from '../../components/header'
import EnterpriseController from '../../../controllers/EnterpriseController'
import CardTop from './CardTop'
import EnterpriseList from './EnterpriseList'
import APP_COLORS from '../../../utils/AppColors'


let that = null
export default class EnterpriseScreen extends Component {

    static navigationOptions = ({navigation}) => {
        const {params} = navigation.state;

        return {
            headerRight: (
                 <RightHeaderBar onPress={() => {
                     that.props.navigation.navigate("GalleryCorretor")
                 }}/>
             ),
            headerLeft: (
                <LeftHeaderBar onPress={() => {
                    navigation.goBack()
                }}/>
            )
        }

    }

    constructor(props) {
        super(props)
        this._controller = new EnterpriseController(props.navigation.getParam("enterprise", "{}"), this.onLoadItems.bind(this))
        this.state = {
            enterprise: [{}]
        }
        that = this
    }

    onLoadItems(enterprise) {
        this.setState({enterprise})
    }


    render() {
        const logotipo = this.state.enterprise != null && this.state.enterprise[0].logo != null ? this.state.enterprise[0].logo : {}
        const topics = this.state.enterprise != null && this.state.enterprise[0].topicos != null ? this.state.enterprise[0].topicos : []
        return(
            <View style={{flex: 1, backgroundColor: APP_COLORS.BLUE_LIGHT}}>
                <View style={Styles.top}>
                    <CardTop logo={logotipo} />
                </View>
                <EnterpriseList items={topics}/>
            </View>
        )
    }
}