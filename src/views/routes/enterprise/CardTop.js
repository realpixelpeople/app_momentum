import React, {Component} from 'react';
import {Platform, Image, Dimensions, View} from 'react-native';
import Styles from './Styles'
import { ImageCacheManager, CachedImage } from "react-native-cached-image";
export default class CardTop extends Component {

    constructor(props) {
        super(props)

        this.state = {
            logo: {}
        }
    }

    componentWillReceiveProps(nextProps) {

        if(nextProps != null && nextProps.logo != this.state.logo) {
            this.setState({ logo: nextProps.logo })
        }
    }

    render() {
        const image = this.state.logo != null && this.state.logo['1125x2436'] != null ? this.state.logo['1125x2436'] : ""
        //<Image source={require("../../../img/rivieraStaCristina.png")} style={Styles.imageTop}/> 
     //<Image source={{uri: image}} style={Styles.imageTop}/> 
        return(
            <View style={Styles.top}>
                <CachedImage source={{uri: image}} style={Styles.imageTop}/> 
            </View>
        )
    }
}