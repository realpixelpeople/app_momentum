import React, {Component} from 'react';
import {TouchableOpacity, Image, Dimensions, View, FlatList} from 'react-native';
import Styles from './Styles'
import { AppWhiteCard } from '../../components/cards'
import { AppTextBold } from '../../components/text'
import APP_COLORS from '../../../utils/AppColors'
import { ImageCacheManager, CachedImage } from "react-native-cached-image";

export default class EnterpriseList extends Component {

    constructor(props) {
        super(props)

        this.state = {
            items: []
        }
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps != null && nextProps.items != this.state.items){
            this.setState({items: nextProps.items})
        }
    }

    _keyExtractor = (item, index) => item.id;

    render() {
        return(
            <View style={{ justifyContent: 'center', alignSelf: 'center', flex: 1, backgroundColor: APP_COLORS.BLUE_LIGHT}}>
                <FlatList
                    data={this.state.items}
                    renderItem={this.renderButtons}
                    keyExtractor={this._keyExtractor}
                    numColumns={2}/>
            </View>
        )
    }

    renderButtons({ item }) {
        return(
            <TouchableOpacity onPress={() => 
                item.tipo === "padrao" ? _navigation.navigate("Topics", { topic: item }) :  _navigation.navigate("Events", { events: item })
            }>                
            <View style={Styles.listContainer}>
                    <CachedImage style={Styles.listBg} source={{uri: item.thumb[Object.keys(item.thumb)[0]]}} />
                    <AppWhiteCard style={Styles.listCard}>
                        <AppTextBold style={Styles.listText} fontColor={APP_COLORS.BLUE_TEXT} fontSize={18}>{item.nome}</AppTextBold>
                    </AppWhiteCard>
                </View>
            </TouchableOpacity>
        )
    }
}
