import React, {Component} from 'react';
import APP_COLORS from '../../../utils/AppColors'
import { AppWhiteCard } from '../../components/cards' 
import {Platform, Image, Dimensions, View} from 'react-native';
import { AppText, AppTextBold } from '../../components/text';
import { LeftHeaderBar } from '../../components/header'
import AppWhiteButton from '../../components/button/AppWhiteButton';
const w = Dimensions.get('window').width ; 
const h = Dimensions.get('window').height ;
import { ImageCacheManager, CachedImage } from "react-native-cached-image"; 


//comentatio de cuzao pro git ler
export class TopicsScreen extends Component {

    static navigationOptions = ({navigation}) => {
        const {params} = navigation.state;

        return {
            headerTransparent: true,
            headerStyle: {
                borderBottomWidth: 0
            },
            headerTitle: "",
            headerLeft: (<LeftHeaderBar onPress={() => {
                navigation.goBack()
            }}/>)
        }
    }

    constructor(props) {
        super(props)
        const topic = props.navigation.getParam("topic", {}) 
        this.state = {
            topic,
            videos: topic != null ? topic.videos : null,
            pictures: topic != null ? topic.fotos : null,
            tour: topic != null ? topic['tour360'] : null
        }
    }

    render() {
        const bg = this.state.topic != null && this.state.topic.bg != null ? this.state.topic.bg[Object.keys(this.state.topic.bg)[0]] : ""
        const title = this.state.topic != null && this.state.topic.nome != null ? this.state.topic.nome : ""
        return(
            <View style={{ backgroundColor: APP_COLORS.WHITE, flex: 1, flexDirection: 'column', alignContent: 'center', justifyContent: 'center', display: 'flex'}}>
                <CachedImage style={{ width: w, height: h, resizeMode: 'cover', position: 'absolute', flex: 1 }} source={{uri: bg }}/>
                <View style={{ width: w, height: h, resizeMode: 'cover', position: 'absolute', flex: 1, backgroundColor: "#4949494D" }} />
                <View style={{ alignItems: 'center', justifyContent: 'center', alignSelf: 'center', marginBottom: 40, marginLeft: 16, marginRight: 16 }}>
                    <AppTextBold  fontColor={APP_COLORS.WHITE} fontSize={40} style={{ alignSelf: 'center', justifyContent: 'center', textAlign: 'center', alignItems: 'center'}}>{title}</AppTextBold>
                </View>
                {this.state.tour && this.renderButton("Tour 360°", this.navigateTour.bind(this))}
                {this.state.pictures && this.renderButton("Fotos", this.navigatePictures.bind(this))}
                {this.state.videos && this.renderButton("Vídeos", this.navigateVideos.bind(this))}
            </View>
        )
    }

    navigatePictures() {
        this.props.navigation.navigate("Gallery", { items: this.state.pictures, type: "picture", title: "Fotos" })
    }

    navigateVideos() {
        this.props.navigation.navigate("Gallery", { items: this.state.videos, type: "videos", title: "Vídeos" })
    }

    navigateTour() {
        this.props.navigation.navigate("Gallery", { items: this.state.tour, type: "tour", title: "Tour 360°" })
    }

    renderButton(text, onPress) {
        return(
            <View style={{  width: w * 0.80, height: 80, margin: 10, alignItems: 'center', justifyContent: 'center', alignSelf: 'center' }}>
                <AppWhiteButton style={{ width: w * 0.70, height: 80, alignItems: 'center', justifyContent: 'center' }} onPress={onPress.bind(this)}>
                    <AppTextBold fontColor={APP_COLORS.PRIMARY_DARK} fontSize={25}>{text}</AppTextBold>
                </AppWhiteButton>
            </View>
        )
    }
}