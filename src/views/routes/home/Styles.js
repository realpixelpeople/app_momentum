import { StyleSheet, Dimensions } from 'react-native';
const width = Dimensions.get('window').width ; 
const height = Dimensions.get('window').height ; 


const style = StyleSheet.create({
    container: {
        height,
        width,
        flex: 1,
        position: 'absolute',
        resizeMode: 'cover'
    },
    bottomContainer: {
        position: 'absolute',
        bottom: 0,
        alignSelf: 'center',
        alignItems: 'center',
        width,
        display: 'flex',
        flexDirection: 'column'
    }
})

export default style