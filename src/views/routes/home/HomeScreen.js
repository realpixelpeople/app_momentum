import React, {Component} from 'react';
import {Platform, Image, Dimensions, View} from 'react-native';
import Styles from './Styles'
import {HeaderBar, RightHeaderBar } from '../../components/header'
import {Enterprise} from '../../../values/Strings'
import {AppTextBold} from '../../components/text'
import APP_COLORS from '../../../utils/AppColors';
import { AppCarousel } from '../../components/carrousel';
import HomeController from '../../../controllers/HomeController'
import { CachedImage } from 'react-native-cached-image'
const width = Dimensions.get('window').width ; 
const height = Dimensions.get('window').height ; 

let that = null
export default class HomeScreen extends Component {

    static navigationOptions = ({navigation}) => {
        const {params} = navigation.state;

        return {
            headerRight: (
            <RightHeaderBar onPress={() => {
                that.props.navigation.navigate("GalleryCorretor")
            }}/>
        )}
    }

    constructor(props) {
        super(props)
        this.controller = new HomeController(this.onLoadCards.bind(this), this.onError.bind(this))
        this.controller.callApi()

        this.state = {
            entries: [],
            bgImage: null
        }
        that = this
    }

    onLoadCards(items) {
        this.setState({ entries: items, bgImage: items[0].bg })
    }

    onError(error) {

    }

    render() {
        const bg = this.state.bgImage != null ? { uri: this.state.bgImage } : require('../../../img/bg_home.png')
        return(
            <View style={{flex:1}}>
                <CachedImage style={Styles.container} source={bg} />
                <View style={Styles.bottomContainer}>
                    <AppTextBold fontColor={APP_COLORS.WHITE} fontSize={26}>
                        {Enterprise}
                    </AppTextBold>
                    <AppCarousel 
                        onSnapToItem={(index) => {
                            this.setState({ bgImage: this.state.entries[index].bg })
                        }}
                        navigation={this.props.navigation}
                        entries={this.state.entries} 
                        sliderWidth={width} 
                        itemWidth={width} 
                        refs={(ref) => {}}/>
                </View>                
            </View>
        )
    }
}