read-me momentum:

1) Instalar HomeBrew /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
2) Instalar digitar no terminal: Brew install node
   a) node -v (ver se o node foi instalado corretamente)
   b) npm -v (ver se o npm foi instalado corretamente)

3) Clonar o Projeto.
4) Diretório raiz do projeto digitar: npm install
5) ||                                 react-native link
6) ||                                 react-native run-ios ou react-native run-android


Obs) Necessário ter as respectivas IDES para rodar iOS (Xcode) Android (Android Studio)
