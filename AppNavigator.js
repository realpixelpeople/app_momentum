import React, {Component} from 'react'; 
import { View } from 'react-native'
import UpdateScreen from './src/views/routes/update/UpdateScreen'
import HomeScreen from './src/views/routes/home/HomeScreen'
import { createStackNavigator, createAppContainer } from "react-navigation"
import Colors from './src/utils/AppColors'
import {HeaderBar} from './src/views/components/header'
import EnterpriseScreen from './src/views/routes/enterprise/EnterpriseScreen';
import { TopicsScreen } from './src/views/routes/topics/TopicsScreen';
import GalleryScreen from './src/views/routes/gallery/GalleryScreen';
import SplashScreen from './src/views/routes/splash/SplashScreen';
import ImageScreen from './src/views/routes/gallery/ImageGallery';
import TourScreen from './src/views/routes/gallery/TourGallery';
import VideoScreen from './src/views/routes/gallery/VideoGallery';
import GalleryCorretorScreen from './src/views/routes/gallery/GalleryCorretorScreen';
import EventsScreen from "./src/views/routes/events/EventsScreen";

const MainStack = createStackNavigator({
    Splash: {
        screen: SplashScreen
    },
    Home: {
      screen: HomeScreen,
    },
    Enterprise: {
        screen: EnterpriseScreen
    },
    Topics: {
        screen: TopicsScreen
    },
    Gallery: {
        screen: GalleryScreen
    },
    ImageGallery: {
        screen: ImageScreen
    },
    TourGallery: {
        screen: TourScreen
    },
    VideoGallery: {
        screen: VideoScreen
    },
    GalleryCorretor: {
        screen: GalleryCorretorScreen
    },
    Events: {
        screen: EventsScreen
    },
}, {
    initialRouteName: "Splash",
    defaultNavigationOptions: {
        headerStyle: {
          backgroundColor: Colors.PRIMARY_DARK,
        },
        headerTitleStyle: {flex: 1, textAlign: 'center', alignSelf: 'center'},
        headerTintColor: '#fff',
        headerTitle: <HeaderBar/>,
        headerRight: (<View></View>),
        headerLeft: (<View></View>)
    }
});

const RootStack = createStackNavigator({
    Splash: { screen: MainStack },
    UpdateModal: { screen: UpdateScreen },

}, {
    mode: 'modal',
    headerMode: 'none',
    tabBarColor:  Colors.PRIMARY_DARK 
})
  
  export default createAppContainer(RootStack);